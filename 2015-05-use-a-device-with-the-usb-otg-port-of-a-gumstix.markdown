# Use a device with the USB OTG port of a Gumstix

![my cameras setup](images/otg.jpg)

This post shows how I managed to use a Firefly camera, from Point Grey, with a Gumstix under Linaro. I present how to use a regular mini-usb cable and hack it into an OTG cable, and I show how to make the gumstix accept a device that demands more power that gumstix's OTG port can offer.

If you read this, I assume that you want to plug a device into the OTG mini-usb port of a gumstix product, but nothing was detected by your gumstix. Let me explain a few things.

First, [Wikipedia](http://en.wikipedia.org/wiki/USB_On-The-Go) explains well what is the difference between the regular USB protocol and the USB On-The-Go protocol. With OTG, there is no real master and no real slave when two devices communicate. In fact, the special USB OTG cable you use allows OTG compatible devices to determine if they will play the slave role or the master role. For example, an Overo Firestorm attached to a Pinto-TH is compatible to the OTG protocol. Using the right cable would allow you to tell your gumstix that it's playing the master role so it can thereafter fetch data from plugged devices.

But what if the device you want to plug is not compatible to OTG? And worst: What if you don't have this fancy OTG cable? 

Don't worry folks, you just found the right post!

The answer is simple. If you want to use a regular device, say a camera, as a slave into a gumstix, you can! Oh and by the way, a gumstix is not meant to be slave, so why the heck enforcing the OTG protocol in the first place? I don't know.

The tricky part, which may cause you a bit of trouble, is the fancy OTG cable. But frankly, it's no big deal, as [this post](http://tech2.in.com/how-to/accessories/how-to-make-your-own-usb-otg-cable-for-an-android-smartphone/319982) gives a step-by-step manipulation to make an OTG cable with a regular one. In short, as shown in the picture below, a mini-usb cable's head can be cracked open and hacked to become connector for the master device.

In other words, take the mini-usb head of the cable you will plug into the gumstix, open it, and ground the wire #4. This will tell the gumstix that it is now the master in this OTG connection. The other end of the cable is left unchanged and you plug it in your device.

Easier said than done! You see, the OTG port in the gumstix can only offer [100 mA](http://gumstix.8.x6.nabble.com/USB-OTG-not-working-as-host-td662224.html) to any connected device. This might not be enough for many cameras. For example, the Firefly MV needs around 200 mA. 

How this is solved is simple, but be careful as it may break your hardware... you don't want to see and smell some smoke! We opened the wire and added a third connector with only the +5V and the Ground connected to the other connectors. This new connector can be used to give the power needed by the device so it can now be fully operational... BUT your gumstix won't like it much....

What happens next? Well... still nothing. The camera is plugged back in the gumstix, which don't want to use it because it cannot give sufficient power. 

Here's how to force your Overo to use your device. In a terminal, run the following command before you plug the device.

``` bash
tail -F /var/log/syslog
```

This will show a log stream that will inform you of the name of the device you want to plug. When it's connected, you will see an error that warns that there is not enough power. In my case, the name of the device was "2-1".

Then, the last thing to do is to force your gumstix to use the device even if it doesn't like it. This is done with these commands.

``` bash
cd /sys/bus/usb/devices/usb2/2-1
sudo echo 1 > bConfigurationValue
```

Using sudo might (will) not be sufficient there. Although, you can use your favorite editor as a superuser, or even use the command "tee".

``` bash
echo 1 | sudo tee /sys/bus/usb/devices/usb2/2-1/bConfigurationValue
```

Ta-Dah!! This should allow you to use any device plugged into an OTG port! I hope you enjoyed this post
