# FM Radio Using rtl-sdr

![gnu radio compagnion's fm receiver schematics](images/rtl-FM-Receiver.png)

In this post, I'll summarize my head-first dive into the world of software defined radio by showing how to create a simple fm receiver with GnuRadio Compagnion.

First, let me share with you the useful websites that you need to install everything and get started. The tool I bought was a [NooElec R820T SDR](http://www.rtl-sdr.com/rtl-sdr-quick-start-guide/). Once I got it, I had to [install the toolkit](https://github.com/EarToEarOak/RTLSDR-Scanner) to control the radio tool and to be able to start scans.

I knew from the scans that FM93.3 had a good signal in my city, so I gave it a try. The [OsmocomSDR website](http://sdr.osmocom.org/trac/wiki/rtl-sdr) give a neat example on how to listen to an FM radio using the command line.

``` bash
rtl_fm --help
rtl_fm -f 93.3e6 -M wbfm -s 200000 -r 48000 - | aplay -r 48k -f S16_LE
```

But all this is not very verbose on how things work. So I then installed the [GNU Radio Companion Software](http://gnuradio.org/redmine/projects/gnuradio/wiki/GNURadioCompanion#Installation) and started to try to figure out the components that are needed in extracting an FM signal. As a side note, [this git](https://github.com/csete/gnuradio-grc-examples.git) contains good examples, but they are outdated, so many blocks are deprecated. Still, it is possible to understand how to repair them to a compilable state.

The best examples I have found was ettusreasearch's [tutorial on how to build an FM receiver](https://www.youtube.com/watch?v=KWeY2yqwVA0) and [v3l0c1r4pt0r's Instructables](http://www.instructables.com/id/RTL-SDR-FM-radio-receiver-with-GNU-Radio-Companion). Thanks to the latter, I made the program detailled in the image below.

![gnu radio compagnion's fm receiver schematics](images/rtl-FM-Receiver.png)

Ok! Let's translate this stuff for the people who, like me, are new to this.

![Source Block](images/rtl-0-source.png)

The "RTL-SDR Source" block represents our radio usb key. Various GUI Sliders have been made to fetch variables from the user of the program in order to set the reception frequency (rx_freq) and tge gain of the radio frequency (rf_gain).

Once the signal is aquired, we need to apply a low pass filter on it [at a standard 100kHz](https://en.wikipedia.org/wiki/FM_broadcasting). This is to filter out undesired frequencies. We also set the decimation (to obtain a digital signal) to a rate that will be low enough so that our sound card will be able to treat it. As for the transition value, I made a slider so that you can play with it. A shorter transition means a more complex filter that will cut any unwanted signal while adding more noise at the end of the captured frequencies, so a longer transition is good here for me because there is a gap big enough between the fm signals.

![Low-Pass Filter Block](images/rtl-1-lowpass.png)

At this point, we have acquired the desired modulated signal. We need to demodulate it. [This blog](http://electronics.stackexchange.com/questions/39796/can-somebody-explain-what-iq-quadrature-means-in-terms-of-sdr) led me to [this nice video](http://youtu.be/h_7d-m1ehoY?t=3m) which explain very well what is a quadrature rate. For the newcomers (like myself), a quadrature rate is the decoding rate of the audio signal. Put it too low, and the digital signal will be too coarse to recognize any music (so you'll end up with something between poor quality music and averaged white noise). Put it too high, and your sound card will not be quick enough to parse the signal. Mine was working without any problems at 250 kHz.

![WBFM Demodulator Block](images/rtl-2-wbfm.png)

Finally, we need a resampler block. It's easy to understand here: you have a music signal coming in at 250 kHz from your sound card, and you want to stream it in a 48 kHz audio sink (your speakers). I made that block so that it adjust it interpolation and decimation value automatically.

![Resampler Block](images/rtl-3-resampler.png)

The last block is a multiplier. It's the block that affects the output volume. A little GUI slider, and you're done!


Here's the [GNU Companion file](downloads/rtl-fm-receiver.grc) for this little project!

Enjoy!
