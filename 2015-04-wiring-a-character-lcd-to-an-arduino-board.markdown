# Wiring A Character LCD To An Arduino Board

![Arduino LCD](images/arduinolcd.jpg)

The post's title says it all! I just bought an arduino and I'm currently learning how to use it. This article present a summary of a post I found showing how to connect an LCD to an arduino board.

For those who want to see [the original tutorial](http://learn.adafruit.com/character-lcds/wiring-a-character-lcd), I recommend it as everything is well explained with a lot of pictures.

For those who want a summary, here it is. 

You need an 16x2 LCD, a 10K potentiometer, a 16 pins long header, a breadboard, a small bunch of wires and and arduino (duh!).

1. Solder the header to the LCD
2. Provide power to the breadboad using the +5V pin and a GND pin of your arduino
3. LCD backlight: Connect LCD16 to ground and LCD15 to +5V. Be sure that your LCD includes a resistor for the LED backlight
4. LCD's logic: Connect LCD1 to ground and LCD2 to +5V
5. Potentiometer: It has 3 pins. Connect the middle one to LCD3, left/right one to +5V and right/left one to ground
6. Connect LCD4 to Arduino's digital #7. LCD4 is RS, i.e. it's the pin used by arduino to send commands to the LCD.
7. Connect LCD5 to ground. This pin is RW, i.e. the direction pin. You won't need it if you only use LCD as a display screen.
8. Connect LCD6 to Arduino's digital #8. LCD6 is the enable pin that tells the LCD when data is ready for reading.
9. Data pins: LCD14,13,12,11 to Arduino's digital #12,11,10,9, respectively.
10. You're done and you should have the unnecessary data pins LCD7,8,9,10 unwired.

It's that easy! Now, when you will use the LiquidCrystal library, be sure to instanciate the LCD with the good pins numbers. If you've followed this post, you should only need to type the following:

``` c
LiquidCrystal lcd(7,8,9,10,11,12);
```

Use the examples given by the Arduino IDE to learn how easy it is to manipulate the LCD. 

Enjoy!
