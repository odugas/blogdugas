# Learning TDD with Katas

![Kata concept images](images/kata.png)

I wanted to train my TDD a bit so I started to do TDD Katas and I would like to share the resulting code.

See [Peter Provost's blog](http://www.peterprovost.org/blog/2012/05/02/kata-the-only-way-to-learn-tdd/) to know more about a code Kata. I felt a bit bored about the Bowling Kata and the String Calculator, since I often use them as examples with teams that are learning how to do Test-Driven Development. There is also a great [git repository by Gaurav Arora](https://github.com/garora/TDD-Katas) that presents a nice list of TDD Katas for your teams to practice with, and I really recommend these examples. Nevertheless, I searched the web for new ideas of Katas, so that I would have to think of the list of tests to create by myself. I found two interesting ones that I'd like to share with you all.

The first Kata I did was in order to learn Ruby. I desired to learn how to program in that language, and I thought that getting started in TDD was the best possible way. Anyway, this Kata is all about the game 2048, [a very addicting game](https://gabrielecirulli.github.io/2048/) created by Gabriele Cirulli.

This Kata is a small one that will introduce anyone that does it to simple test cases, but also to the usage of mocks. You first have to code an algorithm that combine a line (spoiler alert: either row or a column should be considered the same thing to that algorithm). You then have to use this algorithm in an object that I called "grid", that accepts the commands of the game (moving up, down, left or right).

Here's [my ruby implementation](https://bitbucket.org/odugas/ruby-kata.git) for this TDD Kata. I also included in this git repository the [String Calculator Kata](http://osherove.com/tdd-kata-1/). These are my first ruby projects (no kidding, the day before I did this Kata I went to [tryruby.org](http://tryruby.org/)), but I'm quite satisfied with the resulting code. I used [minitest](https://github.com/seattlerb/minitest), which looked to me more similar with GoogleTest or JUnit than the other Ruby test frameworks (like rspec). For mocks, I used [mocha](http://gofreerange.com/mocha/docs/Mocha/API.html), which works very well and is very intuitive.

The second Kata is all about the game of battleShip. Be warned, it requires more involvement than your average 30 minutes Kata session, but it's a nice and fun training project. DataGenetics has a nice [blog page](http://www.datagenetics.com/blog/december32011/) about the best strategy to win against your opponent at the Battleship game (and other games too!). We can summarize it here by saying that you need to make use of probability density functions to shoot at the most probable square when you are searching for an enemy boat. Once you have found a boat, you switch from this searching mode to a sinking mode where you shoot near the hit positions in order to sink the ship.

So, what needs to be done for this Kata? Well, in a real battleship game, you have two boards. The first one is your shooting board. This is the one on which you need to implement the two different algorithms. The second board is the one on which you place your ship.

Here's [my Java code for this TDD Kata](https://bitbucket.org/odugas/battleshiphunter). I chose Java because it's a language that I have experience with, but I needed a refresh using it since I've been mostly coding in C++ in recent years. Enjoy! I think that this code is quite decent. What I like about it is the struggle of deciding which objects needs to be mocked, and which ones could be considered as value objects for the sake of tests' readability and simplicity. I used JUnit and Mockito for this Kata.




