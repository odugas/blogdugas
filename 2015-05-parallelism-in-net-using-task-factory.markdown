# Parallelism in .NET - Using Task Factory

Special Thanks to ReedCopsey, Jr. I found that on [his website](http://reedcopsey.com/2010/03/26/parallelism-in-net-part-16-creating-tasks-via-a-taskfactory), and it was very helpful. 

``` c#
// Start our task on a background thread
var task = Task.Factory.StartNew(() => this.ExecuteBackgroundWork(myData) );
// Do other work on the main thread, 
// while the task above executes in the background
this.ExecuteWorkSynchronously();
// Wait for the background task to finish
task.Wait();

// Construct a task scheduler from the current SynchronizationContext (UI thread)
var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
// Construct a new TaskFactory using our UI scheduler
var uiTaskFactory = new TaskFactory(uiScheduler);

// When inside a background task, we can do
string status = GetUpdatedStatus();
// Update our UI
uiTaskFactory.StartNew( () => statusLabel.Text = status);
```
