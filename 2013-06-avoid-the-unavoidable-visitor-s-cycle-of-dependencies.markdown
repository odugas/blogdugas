# Avoid The Unavoidable Visitor's Cycle Of Dependencies

I got a quick tip. I had to implement a visitor pattern (as in GOF's book, "Design Patterns", go read it). After it was done, I suddenly realized that this class had created an enormous cycle of dependencies in my program. It is a problem inherent to the pattern when implemented in C++, C#, Java and in any other static languages.

Fortunately, Uncle Bob found a solution and [posted it](http://www.objectmentor.com/resources/articles/acv.pdf)

Keep code clean!
