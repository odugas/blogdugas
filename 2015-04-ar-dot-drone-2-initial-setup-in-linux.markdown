# AR.Drone 2 Initial setup in linux

I've purchased a new toy, known as the AR.Drone 2. I was attracted by its powerful motors, its two cameras and by its capacity to be controlled via a smartphone. But the real reason why I bought this toy is because we can access to an SDK for free!

![AR.Drone 2.0](images/ar-drone2-box.jpg)

Well, sure its fun to make it fly with an android phone or with an iphone. It can do flips, record videos and its stabilization system is effective... But looking at the SDK, I discovered that it also allows us to make the drone fly with a gamepad.

Ultimately, I want my drone to fly autonomously, but first I decided to put my effort into making it fly with a gamepad on linux.

I first downloaded the sdk at [ardrone.org](https://projects.ardrone.org/). You have to register, but its free. In the sdk, there is a wonderful pdf guide on how to use the AR.Drone 2 correctly. At the end of the guide, there is a "Tutorials" section. I suggest you start there.

To make it fly with a gamepad, you first need to go at "sdk"/Examples/Linux and then enter the make command. You will have a couple of libraries to install, as explained in the guide. 

Then, you should install joystick:

``` bash
sudo apt-get install joystick
```

This way, you will have access to some useful commands. The two you will need to use is "lsusb" and "jstest". 

**lsusb** is used to see the id of your gamepad. This way, in the xml file of the gamepads configurations for the AR.Drone, you will be able to add a device entry. 

This is the place where I encountered my little problem. the lsusb told me that my gamepad id was 0810:0001. That meant that 0810 was the VendorID and 0001 was my device id. In the guide, it was written to get the id of the device with lsusb, which should be an integer, and then put it in the xml file. After a bunch of tries, I discovered that the vendor ID and the device ID were in fact hexadecimal numbers. So I converted 08100001 to decimal, and the result value was the one to put in the config file.

The other useful command, **jstest**, is used to see the id of every button on the gamepad. It's an easy to use method, once you understand that the device name looks like /dev/input/js*. I just had to type: 

``` bash
jstest --normal /dev/input/js0
```

The only last thing to do is to set this gamepad as the default one, and once you start the application you should instantly press the Emergency Button on the gamepad. If the leds under the drone are flashing [red|green], it means that you can start to fly!

The next step for this project is to be able to grab raw captures from the cameras and to make the drone detect coloured squares. Then, I'll implement a gamepad simulator in order to make the drone to follow the squares autonomously. But that will be for another post!
