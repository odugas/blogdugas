# Wake-On-LAN With A Stubborn Router Using An Arduino

This post is a tutorial that describes what needs to be done to remotely boot up your computer. When I did it myself, I had a stubborn router that did not allow the reception of the magic packet from the internet, so I used an arduino board with an ethernet shield to act as a LAN relay.

Before I begin, I just want to say that nothing in this post is new. I just recycled the information from the web, verified its correctness and integrated all my finds into a single and simple post.

OK! Now, the first thing to do is ensure that your motherboard supports Wake-on-LAN, which is mostly the case if you have a modern motherboard. To make this verification, just go in your motherboard's BIOS. You should try to look for something like 'PCI Power Up' or 'Power On by PCI Device'. If you find this option, just ensure it's enabled, and you are good to continue with this tutorial. Just boot on your favorite operating system.

If your prefered OS is Windows, you should do a little google search, because I prefer Linux systems. However, I googled a bit and found this nice [tutorial for Windows 7](http://windows7-issues.blogspot.ca/2011/03/wake-on-lan-wol-for-windows-7-made-easy.html).

If your prefered OS is MAC, you should do a little google search, because I prefer Linux systems. Still, here's a nice link I found for [Wake-On-LAN enabling with OS](http://its.uiowa.edu/support/article/910).

Now, I was interested in Linux systems, so [here's what one needs to do](https://wiki.archlinux.org/index.php/Wake-on-LAN).  

First you will need ethtool to manually switch on the Wake-On-LAN feature:

``` bash
sudo apt-get install ethtool
```

You then need to query the driver to see if it's defaulted to 'on' :

``` bash
sudo ethtool eth0 | grep Wake
	Supports Wake-on: pumbg
	Wake-on: g
```

If you see 'pumbg' for the option 'Supports Wake-on' and a 'g' at 'Wake-on', then everything should be just fine. But in case you see something else, here's what you need to do:

``` bash
sudo nano /usr/bin/wakewol
```

Add the following code in this file:

```bash
#!/bin/bash
ethtool -s eth0 wol g
```

Then edit the following file:

```bash
sudo nano /etc/network/interfaces
```

And append at the end of it the following line:
``` bash
post-down /usr/bin/wakewol
```

The next thing to do is to note down the MAC address of your ethernet card. A MAC address looks like a1:b2:c3:d4:e5:f6

``` bash
ifconfig eth0 |grep HWaddr
```

At that point, the Wake-On-LAN will activate next time you reboot, but if you want to avoid the useless Reboot before testing everything, type in the bash command :

``` bash
ethtool -s eth0 wol g
```

Now shutdown your computer. You will now test using another Linux PC connected to your home network. Your pc should boot after you do the following :

``` bash
sudo apt-get install wakeonlan
wakeonlan a1:b2:c3:d4:e5:f6
```

If it works, great! Now you need to boot your PC via a remote place. What needs to be done?

If you don't what to remember the IP address of your router (which is definitely the case, since it can change anytime), you need to get a dynamic DNS. That's a web address that will act as a relay that will always know what is the IP address of your home's router. The address will look something friendly like 'myAddress.dyndns-home.com'. I invite you to get a free dynamic dns. There's a couple of websites that offer this service, such as:

* www.noip.com
* dnslookup.me
* dyndns.com
* and so on

When this is configured, you need to edit your router's settings to open the port number 9 (default Wake-On-Lan port) and also lock the MAC address of the PC that will boot with a static IP address in a dhcp table. After that, it SHOULD be fine and you would normally be able to remotely boot your PC. 

However, it did not worked for me. I also tried to redirect incoming packet from port 9 to a broadcast IP address with no success. My router is indeed a stubborn one. I tried a lot of things, like hacking the javascript manager to let me tell the router to broadcast, but it did not do the trick. That's exactly why I thought about my Arduino!

A good alternative when your router don't want to broadcast the magic packet and don't want to send a packet to your PC because it's turned off (even if it's recorded on the dhcp table) is to have a computer that would act as a relay. You would then call the relay, which you automatically broadcast the magic packet in your LAN network, which would boot your pc. 

I had an arduino nearby with an ethernet shield, so here's what I did. I plugged the arduino to my router using an ethernet cable, and I made it run the following code :

``` c
#include &lt;SPI.h&gt;
#include &lt;Ethernet.h&gt;
#include &lt;EthernetUdp.h&gt;

byte arduinoMAC[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress arduinoIP(192,168,1, 177);
unsigned int arduinoPort = 10009;
unsigned int wolPort = 9;
IPAddress broadcastIP(255,255,255,255);

EthernetUDP udp;

#define MAGIC_PACKET_SIZE 102 //102 = 0xFF 6 times and 16 times the MAC adress of the pc to wake
byte packetBuffer[MAGIC_PACKET_SIZE];

void setup() {
  Ethernet.begin(arduinoMAC, arduinoIP); // Connect to the local network
  udp.begin(arduinoPort); // Starts listening to the router's redirection port
}

void loop() {
  forward_wol_magic_packet();
  delay(100);
}

void forward_wol_magic_packet(){
  int packetSize = udp.parsePacket();
  if(packetSize == MAGIC_PACKET_SIZE) {
    udp.read(packetBuffer, MAGIC_PACKET_SIZE); //transfert read data to the packetBuffer

    // redirect udp packet to broadcast address
    udp.beginPacket(broadcastIP, wolPort);
    udp.write(packetBuffer, MAGIC_PACKET_SIZE);
    udp.endPacket();
  }
}
```

Doing that, and after configuring port redirection in my router to redirect incoming messages through it, this solution worked perfectly. The arduino just listens to the udp port number 10009, and when it receives the magic packet, this packet is broadcasted in my LAN network. The command to make the pc boot then becomes :

``` bash
wakeonlan -i void.dyndns-home.com -p 10009 a1:89:a5:31:d8:d9
```

Enjoy!
