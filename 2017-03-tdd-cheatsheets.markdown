# GoogleTest and GoogleMock CheatSheets

I made a cheatsheet to help people get into GoogleMock, and I want to share it, along with the links and cues you need when you'll get started with GoogleTest.

Here's the links to [GoogleMock's CheatSheet](https://drive.google.com/open?id=0Bzrl2Al8APAFRXZmcGJGNUVqQUE) and to [GoogleTest's CheatSheet](https://drive.google.com/open?id=0Bzrl2Al8APAFNnpNLWFFVnlrbDg)

In order to use GoogleTest, have a look at the [Advanced Guide](http://letmegooglethat.com/?q=googletest+advanced+guide&l=1). I recommend reading the sections talking about "Sharing Resources Between Tests in the Same Test Case", "Global Set-Up and Tear-Down".

As for the syntax for mock headers, I simply recommend you automate everything. Have a look at gmock_gen.py from [GoogleTest's git](https://github.com/google/googletest). Especially if your team handles multiple programming languages, having to bother about GoogleMock's headers syntax is a concern you do not want.

Enjoy!
