# Make a Raspberry Pi boot on a Hard Disk Drive

![Raspberry Pi and Hard Drive](images/raspi-harddrive.jpg)

A couple of months ago, I made [a web server with a Raspberry Pi](2015-05-make-a-web-server-with-raspberry-pi.markdown). This current blog is hosted on my Pi right now. Unfortunately, SD cards have limited read-write access, which made my blog crash lamentably. The solution that I present in this post will show how to make your Pi boot and work exclusively on a HDD, which is way more reliable!

Well! Here I am, after almost three months of down time because I did not have any time to repair my server.

Ok so here's the problem. You have a Raspberry Pi, with a class 4 SD card. Everything works just fine. Suddenly, after 10k to 100k read-write actions, everything start crashing down and your Pi doesn't respond anymore.

So what should you do? Use another SD card, and the problem will happen again. Use a USB key would do the same thing maybe after a little more while. If your website uses a database of some sort, you have no choice but to use a hard disk drive?

But the real problem is, how do you tell the Raspberry Pi to NOT boot on an SD card? Can you make it boot on an external drive?

To use an hard disk drive with my Pi, I got most of the help I needed [here](http://raspberrypihobbyist.blogspot.ca/2013/07/running-from-external-hard-drive.html). Below, I provide the steps needed if you use the NOOBS version of the Raspberry Pi OS.

First, take a hard drive and, using gparted, create an ext4 partition for the root target and a swap partition. Since the hard drive will be plugged in your Pi only, I suggest you use all drive. On my PC, the drive I prepared to use with the Raspberry is /etc/sdg (sdg1 for ext4 and sdg2 for swap). Use the following command to initialize the swap partition:
``` bash
mkswap /dev/sdg2
```

Download [the NOOBS OS for the Raspberry Pi](http://www.raspberrypi.org/downloads). Follow the steps provided by [the quickstart guide](http://www.raspberrypi.org/wp-content/uploads/2012/04/quick-start-guide-v2_1.pdf).

Boot the Pi and make NOOBS install itself. Do not overclock, as Pi is already fast enough to run, and it will decrease the probability of data corruption. I crashed twice my Pi, and once because of the fact I overclocked. So proceed at your own risks.

When the Pi is booted up, make it up to date. This will take a while.
``` bash
apt-get update
apt-get upgrade
rpi-update
reboot
```

Ok now, close the Pi, remove the SD card and plug it in your PC (the one with the drive you want to use. Locate the root partition of the SD card using gparted. Mine was /etc/sdf6.

Hard copy the root partition from the SD card to your hard drive's ext4 partition. It's a hard copy to avoid cp problems. This takes long, just be patient. After the copy you'll get a message telling the bytes copied.
``` bash
sudo   dd   if=/dev/sdf6   of=/dev/sdg1   bs=512
```

Then, verify that dd proceeded without any error. If there are some error, pressing 'y' will solve the problem. If there is way too much errors, as it first did with me, just reformat you're hard drive and start over the copy. You verify the hard drive with the following command:
``` bash
e2fsck -f /dev/sdg1
```

Because your card was 4Gb size or more instead of the real size of your hard drive, you have to resize the ext4 partition so you all of it is filled and useable.
``` bash
resize2fs /dev/sdg1
```

Now, you must know that your Raspberry will always need an SD card to boot. The thing is that it needs the BOOT partition on the card to give it the parameters needed in order to get root's location. So, you've guessed it, we have to edit these parameters. Find the boot partition in the SD card and edit the file named /boot/cmdline.txt.

What do we need to edit? Two things. First, there's a parameter called root=/dev/mmcblk0p2. You have to replace the value of this root partition by the one of your hard drive. For example, mine had two partitions, sdg1 (root) and sdg2 (swap). When you will plug the drive on your pi, it should respectively become sda1 and sda2. So, put /dev/sda1 instead of /dev/mmcblk0p2. The second thing you have to do is to add a parameter at the end of the parameters line to tell your Pi to wait for your external hard drive to load on the PI before trying to mount the root partition. The parameters you have to write are "bootdelay rootdelay". You don't have any argument, just copy the two words between this quote at the end of the parameters' line.

Ok now, you have to setup the new root device's fstab to configure the root and swap partitions.
``` bash
sudo mount /dev/sdg1 /mnt/anything
sudo nano /mnt/anything/etc/fstab
```
Change the root device /dev/mmcblk0p2 to be /dev/sda1 and add /dev/sda2  as  a swap partition by adding this line.
``` bash
/dev/sda2    none      swap    sw           0       0
```

Finally, you have to make sure your Raspberry won't have the brilliant idea of using a swap file it normally uses instead of the swap partition.
``` bash
rm /mnt/anything/etc/rc2.d/ S02dphys-swapfile 
```

Make sure everything is transfered from memory to disk before you remove both SD card and hard disk drive. I love this command, since it tells you exactly when you can remove a usb device without breaking it.
``` bash
sync
```

That's about it! It's not quite an easy task to do, but with this post you should get through it without any scar. Again, if you want to know how to make a web server out of your Pi, visit [my older post](2015-05-make-a-web-server-with-raspberry-pi.markdown).

Enjoy!

### EDIT

My Pi get frozen once in a while. [This post](http://www.bartbania.com/index.php/raspberry-pi-watchdog-timer/) will show you how to install a watchdog that will reboot your Pi in case of any ceisure.
