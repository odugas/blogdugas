# Use A Raspberry Pi As A Music Server
![ My home setup](images/homesetup.jpg)

I don't like to spend money on new technologies if I already have all at home to do it myself. In this post, I will give all the information you need to use your Raspberry Pi as a music player that can be remotely controlled.

I had a problem in my living room. I have an old CRT television and a >15 years old stereo system. This kind of electronics just won't die, just like any electronics used to be. With an RCA splitter, I managed to plug in 2 gaming consoles (including a SNES, yay!), a DVD-VHS player, and some 1/8 jacks to listen to sounds coming from laptops. Unfortunately, my old stereo system could not play music from a USB key and my PC is in another room. When inviting friends home, in order to listen music, I would have had to rip a CD, or to bring in a laptop near the sound system. Well... that just sucked.

So after some researches, and having my Raspberry plugged right next to my television, I found the perfect solution for me. 

[Music Player Deamon](http://www.musicpd.org/) (MPD) is "a flexible, powerful, server-side application for playing music". It's just what you need if you want to stream music over any network. It also happens to allow one to start music on the server at a distance. It works well on a Raspberry Pi. I have memory of having some troubles starting the MPD for the first time, but the error messages received were clear enough to quickly ask google for the solution.

I then plugged an audio jack from the Raspberry into my splitter. When this was done, I filled my RasPi with all my music [using a hard-drive](2015-05-make-a-raspberry-pi-boot-on-a-hard-disk-drive.markdown) and I [downloaded a free MPD client for my Android Phone](http://lmgtfy.com/?q=best+mpd+client+android).

Now, whenever friends come to visit me, I just grab my phone and listen to the music I want to hear without having to make my guests wait.

Sadly, I discovered that my Raspberry started to crash systematically a day or two after I used MPD. This was a huge problem, since I host this blog on it. Fortunately, my salvation was in a truly wonderful blog entry explaining [how to install a Watchdog Timer](http://www.bartbania.com/index.php/raspberry-pi-watchdog-timer/) so that the Pi will try to reboot my itself if any problem occurs. Since I installed the watchdog, I've never had to reboot my Pi again.

Enjoy!
