# Make A Web Server With Raspberry Pi
A Raspberry Pi is a cute gadget. It's a computer that fits in your hand! I just bought one and I asked myself if it was possible to build my own server on this little piece of technology?

Well, it certainly does, since this current Octopress blog is running on it right now!

In this post, I quickly give the links for anyone to do the same, and I solve the bugs I encountered while following the load of tutorials.

Ok! First of all, here's the best [how-to guide I found](http://www.wikihow.com/Make-a-Raspberry-Pi-Web-Server) about hot to make a Raspberry Pi web server. I suggest you follow it, but read the following before to avoid doing the same mistakes as I did.

When you will get to the part where you have to install mysql, you might get some error messages that should indicate that the rest will fail. I don't remember the french forum I fell into, but here's what I found.

The line that could show an error message:
``` bash
sudo apt-get install mysql-server mysql-client php5-mysql
```

What to do if a problem occurs. This will flip a somehow important bit, but I did not look into it more since it solved my problem.
``` bash
sudo chmod 1777 /tmp
```

Also, after I encountered the problem, I tried to remove mysql to install it again, but it failed saying that my sd card was full. [This blog](http://databaseblog.myname.nl/2013/01/how-to-install-mysql-succesfully-on.html) helped me to reinstall everything back. You just have to do: 
``` bash
sudo apt-get purge mysql-server-5.5
sudo rm -rf mysql/
sudo apt-get install mysql-server-5.5
```

Another problem I faced was with the raspian distribution installed on my Pi. When I was trying to do:

``` bash
sudo apt-get update
sudo apt-get upgrade
```

Linux was still telling me that one application needed to be upgraded. I found that it was omxplayer (very nice movie player, if you ask me! I use it to play movie on my old TV). When I tried to manually upgrade it, I got perl errors telling that my locales settings were not right. [This post solved my problems](http://www.ardupi.com/2013/01/raspberry-pi-raspbian-locale-settings.html) so thanks to [Ray McKinnes](https://plus.google.com/115672991723240002303/posts)!

Finally, the last problem I faced was that all the last parts from the tutorial, starting with the editing of /etc/passwd, did not work at all. When I tried to comment my Raspberry Pi User line, pi was no longer recognized and if I did not comment this line, I was receiving the error telling that Pi was in used even if it was not by me since I was logged in as root.

I've started looking at the [manual](http://linux.die.net/man/8/usermod) to see if there was a way to force things a bit. Some forums also told me to create a new user instead of trying the manipulations I was trying to do. Finally, I found a [post from NixCraft](http://www.cyberciti.biz/tips/howto-linux-kill-and-logout-users.html) that explained how to kill the process running as Pi User. It's quite simple, just do:

``` bash
pkill -KILL -u pi
```

And then the initial tutorial will finally work flawlessly without commenting Pi User's line in /etc/passwd and you will be up and running!

Now that I had a server with my Raspberry Pi, I had to learn how to make virtual hosts with Apache, and [this tutorial](http://www.adminnation.com/2011/08/23/running-multiple-websites-using-apache/) was very complete and very clear. I recommend it.

The last step is to buy a domain name, and then to make your dynamic IP adress linked to this domain. There are load of information to be found easily with google, so I will not hold anyone's hand on that part. Google it yourself, you'll see it's very dead simple!

EDIT - Don't forget to comment out the "RedirectMatch ^/$ /apache2-default/" from your website's config file in /etc/apache2/sites-available/mywebsite if you get some weird 404 Not Found errors!

When you have you're website up and running, here's how to set up and configure phpmyadmin, so you have a pretty web interface for editing your database.

``` bash
sudo apt-get install phpmyadmin
sudo nano /etc/apache2/apache2.conf

# add the following command at the end of the file
Include /etc/phpmyadmin/apache.conf

sudo nano /etc/phpmyadmin/apache.conf
# find the line that starts with Alias (should be line 3)
# change /phpmyadmin to the address of your choice

sudo service apache2 restart
```

Also, if you get the following error during apache restart:
"Could not reliably determine the server's fully qualified domain name"
Solve this error message with the following manipulation.
``` bash
sudo nano /etc/apache2/apache2.conf
#add the following command anywhere in the file (or at the end)
ServerName "myDomainName.com"
```

Enjoy!
