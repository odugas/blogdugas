# About This Blog

To put you in context, I found myself searching the web to find informations or programming solutions in order to solve problems that I already encountered.
So I thought &quot;Hey, I could create a blog to store any bug or code that I believe could be useful in the future for me or somebody else!&quot;
Take a look inside and share if you liked anything in this blog!

My name is Olivier Dugas. I'm a senior software engineer, with a Masters in probabilistic robotics and a MBA in software companies management.

Also, I'm a software professional. By professional, I mean that I worship code quality. By code quality, I mean code that read like well written prose, which is fully covered by tests, and which is structured in a clean architecture.

What I can do for a company:

* I can help your compagny produce software with better quality, to ship it faster and more consistently.
* I am a good software developper, which means that I have solid background in automated tests and flexible software architecture design.
* I code clean as I learned from the best ([Elapse Technologies](http://www.elapsetech.com/))
* I know many languages. Name it, I can program it. Otherwise, give me a week and you will be impressed, I mean it

Contact me via [LinkedIn](http://ca.linkedin.com/in/odugas/) if you have any question about me, if you want to hire me or if you want me to get some hard work done!
