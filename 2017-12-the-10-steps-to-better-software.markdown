# 10 Steps to Continuous Delivery
It's been 18 years since Joel Spolsky's [12 steps to better code](https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/) and since back then, technologies, tools and techniques have improved greatly. In this post, I want to share with you what I think is the checklist any software company should comply with nowadays.

1. Do you version-control everything?
2. Do you have automated tests?
3. Can you make a build in one step?
4. Can you run all the automated tests in one step?
5. Are the QA and the developers working as a single team?
6. Do you tag every shippable version?
7. Do you work at a sustainable pace?
8. Can you ship your products in one step?
9. Can you confidently ship today?
10. Do you continuously improve your processes?

I regularly coach and accompany teams to get through these milestones one by one. Just like with The Joel Test, these are questions that you can get a quick yes of no for each one. Give your team 1 point for each "yes" answer. From there, iteratively improve your team's workflow until you can reach a perfect score!

Granted, these questions tell nothing about the user experience, which in my opinion is the single most important thing we should care for if we want the products to sell and the company to succeed. Still, these questions will validate if your team is flexible to changes that naturally occur in the software engineering field.

Enjoy!
