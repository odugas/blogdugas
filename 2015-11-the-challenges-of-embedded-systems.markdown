# The challenges of Embedded Systems
I gave a seminar in French at Laval University, which will give rise to a 2 days course that I will start giving in Montreal in 2016. This post offers a download link.

During this seminar, I discussed with the attendees about:

1. Embedded Systems
    * The different kinds of embedded systems
    * The differences and constraints they each bring
    * A comparison with regular software development
2. Continuous delivery applied to Embedded
    * Source Code Management
    * Version Management
    * Continuous Integration
    * TDD with embedded
    * Test automation particularities in embedded systems
    * Compilation modes and IDEs
    * Scripting in the embedded world
    * Automated deployment vs Continuous deployment
    * Image generation tools
3. Yocto Project
    * A description of the Yocto Project
    * How it works
    * How to use it
    * How to not use it
    * How to automate it

I know, it's not in english. Well... I won't translate it, I'll just make it better someday! Enjoy this in the meantime!

[Download the poster](downloads/Embedded.pdf)
