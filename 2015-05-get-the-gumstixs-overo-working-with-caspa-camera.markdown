# Get the Gumstix's Overo working with Caspa camera
I recently wrote [this post](2015-05-setup-a-gumstix-overo-firestorm-with-linearo-in-sd-card.markdown) where I showed how to get started with the latest linux kernel. Now, I learned that you need to freeze in order to use caspa cameras with the gumstix. So after hell of a search, I finally got everything working. I describe what I have done in this post.

First of all, the following links explain us that the driver for the camera has changed for the worse if you use the new kernels, just in case you don't believe me.

http://permalink.gmane.org/gmane.linux.distributions.gumstix.general/66090
http://comments.gmane.org/gmane.linux.distributions.gumstix.general/66089
http://www.oz9aec.net/index.php/gumstix-overo/452-work-continues-on-the-omap3-vpu

But believe me, I tried with a Linaro 3.5.0 kernel by doing the following:

First, I used [this script](https://github.com/RobertCNelson/tools/blob/master/pkgs/aptina-media-ctl-n-yavta.sh) to install media-ctl and yavta. I then found [here](http://gumstix.8.x6.nabble.com/Caspa-on-Overo-FE-3-5-kernel-td4966351.html) and [there](http://gumstix.8.x6.nabble.com/Caspa-FS-on-Yocto-gumstix-td4966530.html) how to use these commands to capture an image. I could have used mplayer like [the official wiki told to](http://wiki.gumstix.org/index.php?title=Caspa_camera_boards), but I was getting either a fully green capture, or a solid black image. 

Unfortunately, working with media-ctl and yavta only produces a .bin image and I could not find any tool that could use this format of image, since it looks more like a video file, and any tool that I used to read it returned invalid format errors and such.

Some link was also suggesting to do some camera streaming with gstreamer, but I could not spare more time searching about that. Another one told how to [load the .bin image into matlab](http://compgroups.net/comp.soft-sys.matlab/writing-a-bin-file-to-tiff-or-jpeg-file/971184), but even if it succeeded, the format of the image was gibberish to me. I also considered using ROS to use the camera with the Linaro kernel, but [the FAQ from ROS website](http://answers.ros.org/question/27556/gumstix-caspa-camera-and-ubuntu-1104/) told me it's not ready for now, and that I would have greater luck by freezing the kernel to an older specific version.

Now that I convinced most that caspa cameras are kind of crappy to use right now, here's the steps one needs to follow to get started fast with a fully functionnal overo fire (or air) that have wifi enabled and that can take pictures.

The best tutorial is [from the paparazzi.enac.fr wiki](http://paparazzi.enac.fr/wiki/Dev/Caspa). They refer to [google code's website](https://code.google.com/p/beagleboard/wiki/LinuxBootDiskFormat) to prepare an SD card (less that 2Gb needed, I used a 4 Gb). I enjoyed how the usage of fdisk is well described, but other tutorial for formating the SD card can do, [like the side-official tutorial on gumstix's website](http://gumstix.org/create-a-bootable-microsd-card.html). As for the paparazzi tutorial, once everything is setup and that you booted on the Overo, in order to show an image on your screen, they suggest you do the following: 

``` bash
overo login: root
export DISPLAY=:0.0
mplayer tv:// -tv driver=v4l2:device=/dev/video0 -x 480 -y 272 -vo x11
```

Unfortunately for me, I don't have any lcd screen for now, so if you just want to save an image on disk, use the "Test" section of [gumstix's wiki](http://wiki.gumstix.org/index.php?title=Caspa_camera_boards):

``` bash
overo login: root
export DISPLAY=:0.0
mplayer tv:// -vo png -ss 1 -frames 1 -loop 1 -tv driver=v4l2:device=/dev/video0
```

Ok. Now we have an old frozen kernel, stuck in version 2.6.34. and we don't have access to wifi. I had about half of my day searching in forums to get wifi working. At my university, we have the international "eduroam" wifi, so I guessed that it would be straightforward to set in wpa_supplicant.conf a simple LEAP connection, but it did not worked. So here's the things I did that made wifi wake up for no reasons:

First, edit /etc/network/interfaces:

``` bash
#
#snip
#
# Wireless interfaces
#
#allow-hotplug wlan2
auto wlan2
iface wlan2 inet dhcp
      pre-up wpa_supplicant -Dwext -iwlan2 -c/etc/wpa_supplicant.conf -B
      down killall -q wpa_supplicant
#
#snip
#
```

Notice that I used wlan2. If you wan't to know which name has your wireless interface, just type the following in the console:
``` bash
iwconfig
```

In order to ensure that the antenna is working, enter this command:
``` bash
iwlist scan
```

Also you need to edit the /etc/wpa_supplicant.conf file. As I said earlier, I had some trouble getting a wifi connection with eduroam. I solved using a more generic description in the wpa_supplicant.conf file, that looks like this:
``` bash
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
eapol_version=1
ap_scan=1
fast_reauth=1
 
network={
        ssid="eduroam"
        scan_ssid=1
        key_mgmt=WPA-EAP IEEE8021X NONE
        pairwise=CCMP TKIP
        group=CCMP TKIP
        eap=LEAP PEAP
        identity="foo@ulaval.ca"
        password="bar"
        phase1="peaplabel=0"
}
```

At last, just type the following commands and cross your fingers hoping the last one will show you an IP address:
``` bash
ifdown wlan2 && ifup wlan2
ifconfig
```

Wow! Finally. Well that was hard! I seriously spent too much time trying to connect to the wifi. The last steps are optional, since an ssh server and client is already installed so you can connect to the Overo. Still, these could upgrade a bit your security. Enjoy!

``` bash
opkg update && opkg upgrade
passwd
```
