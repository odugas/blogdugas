# Setup a Gumstix Overo Firestorm with Linearo in SD Card

Last week, I just received some gumstix stuff. I noticed that the documentation is quite sparse, so I will just tell in this post every problems I went through, plus gives you the links needed to get up and running fast.

First of all, if you're new to the gumstix thing, know that I was too just a week ago, so I'll try to cover it all.

Ok, so what is the gumstix stuff I currently have? I have an Overo Firestorm, which is sort of the cpu part of the gumstix. I have a 16 Gb SD card, another 4 Gb SD card, and I have a chestnut43 that is used to connect to a linux computer, but I don't have chestnut's LCD touch screen. I should have it soon enough tough. My advice here would be to make sure to buy a 4.3 inches with chestnut43 and palo43, and to buy a 3.5 inches with the parts that finish with 35 ;-) 

If you need to know what part does what, there are good introduction videos to be watched [right there](https://www.gumstix.com/gumstix-videos.html

The tutorial that I do recommend gives you the opportunity to use 3 different images with your gumstix. I tried two, namely linaro and yocto project. Linaro is in fact an Ubuntu OS, with ROS already built in. Since the image is 8 Gb, you need to use a 8 Gb or above SD card. Yocto is an OpenSUSE OS and is quite small, so if you're familiar to OpenSUSE and if you don't have access to a big SD-card, Yocto is just for you. 

## How to get started with YOCTO

Here's what you need to do. First, go to the [yocto image download site](http://gumstix.org/downloads/). The files you will need are mostly under "Factory Stable Build", plus any rootfs.tar.bz2 you will find on this actual file.

Then, you need format and load the Yocto OS on a 2 Gb or 4 Gb card. To do that, there's a sort of an alternate tutorial you can use that is much more efficient [there](http://gumstix.org/create-a-bootable-microsd-card.html). Follow this tutorial, and when you get to the "U-boot Environment Refresh, I suggest you get back to [this main tutorial](http://gumstix.org/getting-started-guide/243-booting-your-gumstix-system.html) where it's explained a bit better. You should manage to boot it without any problem. Unfortunately, I did not go further than that since at this point I just learned that Yocto was an custom version of Linux, and in that case it was build to look like OpenSUSE. Since this linux version is not for the moment compatible with ROS, and because I was not familiar enough with OpenSUSE (nor with Yocto at time of writing), I just gave up and I bought a 16 Gb SD card. 

## How to get started with Linaro

This OS for gumstix is quite easier to use I believe. I [downloaded the latest Linaro image](http://gumstix.org/getting-started-guide/241-get-an-image.html), then I created a [bootable SD card](http://gumstix.org/getting-started-guide/242-create-a-bootable-microsd-card.html). Since my card was a 16 Gb and the Linaro only takes 8 Gb, I used GParted in linux to extend my Linaro partition. When the card was prepared, I [booted the gumstix](http://gumstix.org/getting-started-guide/243-booting-your-gumstix-system.html) and connected to it via a simple command in linux. 

At this point, I finally booted with Ubuntu on my gumstix. Now, I needed to connect using the wifi (since Overo Firestorm has wifi and bluetooth, which I believe is pretty nice). Here's the commands that will be useful to connect to a wireless network :

+ ifconfig: Enable your wireless device.
+ iwlist: List the available wireless access points.
+ iwconfig: Configure your wireless connection.
+ dhclient: Get your IP address via dhcp.

But the university where I work uses the international eduroam wireless network, which is using EAP-PEAP. So I needed something a bit different so I edited /etc/wpa_supplicant.conf file in order to get connected upon startup to the wireless network. I suggest you see [this example of a wpa_supplicant configuration file](http://www.lsi.upc.edu/lclsi/Manuales/wireless/files/wpa_supplicant.conf), that is very well documented. In my case, I wrote this in the config file:

``` bash
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
eapol_version=1
ap_scan=1
fast_reauth=1

# EAP-PEAP/MSCHAPv2 configuration for RADIUS servers that use the new peaplabel
# (e.g., Radiator)
network={
	ssid="eduroam"
	key_mgmt=WPA-EAP
	eap=PEAP
	identity="user@ulaval.ca"
	password="foobar"
	phase1="peaplabel=1"
}
```

I then [read a bit about how to use wpa_supplicant](http://www.codealias.info/technotes/wireless_security_wpa/wap2_with_eap-peap_using_wpa_supplicant_and_client_ssl_certificates_linux_setup) in order to know how restart my wireless interface, which was done using the following commands:

``` bash
wpa_supplicant -B -i IFACE -Dwext -c /etc/wpa_supplicant.conf
dhclient wlan0 # I waited a lot there.
```

A quick reboot after that and the gumstix was now connected to the world! Yay! Other then that, there was duplicate sources list entries, which gave me an annoying error when I was doing the update and upgrade commands, but [nothing that had not been explained in askUbuntu](http://askubuntu.com/questions/183007/how-to-fix-duplicate-sources-list-entry-warning)!

Enjoy!

Oh and by the way, I also got Caspa cameras for my gumstix so I will post something soon about it!

**UPDATE**
The gumstix's wiki also contains a great tutorial on how to get a Linaro image with only a 1Gb SD card. So I invite you to visit [this wiki](http://wiki.gumstix.org/index.php?title=Installing_Linaro_Image). Just keep in mind that installing ROS will require more space, and here's [The complementary tutorial to get ROS with Linaro](http://gumstix.org/software-development/246-ros.html).
