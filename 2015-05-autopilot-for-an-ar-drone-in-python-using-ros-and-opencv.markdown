# Autopilot for an AR Drone in Python using ROS and OpenCV
![AR.Drone Autopilot](images/autopilot.png)

I've done a project just for fun with Sebastien Audet recently. In a few days, we used ROS and OpenCV to program an autopilot for the AR.Drone!

[Watch it in action!](https://www.youtube.com/watch?v=YiXqLZ6DTsU)

In brief, we used ROS to connect to the robot, to get a camera feed and to be able to fly it with a gamepad. Then, we converted the camera feed to OpenCV format, and we used the colors of a pink and green target to compute what should be the roll, pitch, yaw and thrust commands to send to the drone. The video above shows the robot to automatically do multiple combination of movements without any human assistance. 

Sure, this is a prototype. The automatic white balance makes it hard to detect good colors outside, and it's fixed camera makes it hard to smooth the movements when it's windy. If we had a more powerful (and costly) robot, we would have done much greater things! Still, not bad for a project completed within 6 days with a budget of $350 of hardware!

I'll share the code with anyone who asks. Nevertheless, I already wrote two posts to help getting you started!

[AR.Drone 2 with ROS and OpenCV: Get Started Quick with Ubuntu or Mint](2015-05-ar-dot-drone-2-with-ros-and-opencv-get-started-quick-with-ubuntu-or-mint.markdown)
[Search For A Color: OpenCV2 Basic Manipulations In Python](2015-05-search-for-a-color-opencv2-basic-manipulations-in-python.markdown)

Enjoy!
