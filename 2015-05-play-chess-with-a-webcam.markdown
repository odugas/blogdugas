# Play Chess With A Webcam
![Play Chess With A Camera](images/chessboard.png)

The project named ChessCam is finally completed! I was wondering if I would one day have the opportunity to play chess against the AI without using a keyboard or a mouse.

The project was initially created as a school project in september, 2012. I was doing the project with my teammate, Yannick Hold. ChessCam principle is simple: Using a webcam, we used the UCI (Universal Chess Interface) to design a chess engine that can interact with Arena Chess GUI 3.0 just like if it was an AI. But, instead of being an autonomous AI, ChessCam uses a cheap webcam to analyse moves played by a human on a real chessboard. ChessCam is developed in Python, with OpenCV.

The webcam can be placed in vertical angle (not necessarily over the board) and the board can be viewed from any horizontal angle. We simply use 4 spots of green tape to ease the chessboard corners' detection. Also, the program analysed detected moves on the board to find what move has occured and if it's a legal move. Furthermore, because some chesspieces are taller, the software considers what we called "dominant cells" to decide if move really occured on a specific cell. It considers EnPassant and castling moves. Unfortunately, because the webcam precision is low, we have not implemented the detection of the chesspiece during promotion. Note that ChessCam can play with the black team as well as with the white team.

Here's a link to the video a game we played with ChessCam as white, against some AI playing black: [youtube](http://www.youtube.com/watch?v=I3F3pqh9LYc)

Also, here's the github for our project: 
``` bash
hg clone https://bitbucket.org/soravux/vision
```

I hope you appreciate! ChessCam is OpenSource!

Oh I forgot to mention... We lost some time trying to plug the engine into Arena Chess GUI. We were able to fetch inputs from the GUI, be we were unable to output played moves. The solution here is to remember flushing the output buffer. Otherwise, Arena don't have access to the move that ChessCam prints in the output stream.

Send me videos if you ever use it! I would love to see this project become something more than "it's just a project for school..."!

### UPDATE
Recently, an honorable man wrote me just to know a bit about how chessCam had to be installed if he wanted to play chess with a webcam against his grandsons online. I've copied my answer below, just in case someone else want to know.

It will be easier to use with windows, since there's a lot of things to install and to configure. Although, we tested our sofware with linux AND windows (We don't have any MAC)

First of all, install chess arena 3.0 (link above, playwitharena.com) and learn how to play chess against people worldwide using a mouse.
THEN, download our latest code release (every python files (.py) located at [bitbucket](https://bitbucket.org/soravux/vision/src)
You will need to install a python compiler [see there](http://www.python.org/download/)
Install OpenCV (to be able to use a webcam with python) [Willow Garage](http://opencv.willowgarage.com/wiki/)

When everything is installed, create an executable file (.bat for windows, or an executable file in linux) that will contain a command line that will run our python files (the main file of our project is GameEngine.py, if you start this file everything will boot up and wait for a go sent by chess arena)
Finally, open chess arena, select the executable file to make the GUI to connect with the ChessCamera, and connect with your grandsons. You will then be able to play with them using a webcam with a real chess board.

Also, I'll give you some tips about how to setup the chess board.

First, you'll need a chess board with 4 green markers on each corners of the board (green tape will do just fine). See attached picture.

Then, you have to use any cheap color webcam and aim the chess board. (the higher the camera, the better. If the camera is too low, the camera will see pieces in multiple squares at a time. The software is robust if pieces are seen on their actual positions and on one square above, but be gentle with it! 

If  the green markers are not detected, you will need to set the threshold for the green color (in the file called BoardFinder.py) 

Also, webcams tend to automatically ajust their level of whiteness. This is bad. You won't be able to make chess cam work if the camera settings change on every image.
If you use windows, I recommend using [amcap](http://amcap.en.softonic.com/)
There a way to download it free via the website, you do not have to purchase it. Configure the whiteness around 3900, max out the contrasts and the level of details in images using this little software. If you use linux, I think there a way to fix the webcam settings using skype.
Remember that every time you unplug the webcam, it resets the settings. 

When the camera will start working using our program, even with amcap, settings seems to change on the first movement detection. To avoid this problem, just shake your hand in front of the camera before playing your first move. 

One last thing: If you only want to debug the webcam's movement detection, you can run the file called ChessCam.py. It will open the webcam, detect the board and print the movements squares on which movement was detected, regardless of if a piece was there or no(movement analytics are computed in state.py, a horribly big file, but since we had a lot of things to do, we did not put much efforts into splitting that file) 
