# Experiments in Quadrotor Formation Flying Using On-Board Relative Localization
![AR.Drones flying in formation](images/dronesfacing.png)

In this post I present a paper that I co-authored. In fact, I only provided some of the code needed to drive the drones, and they added me as a So here's the link to the article.

## Abstract:
Formation flying of aerial robots has many applications, such as surveillance, coordinated transport of heavy objects, convoying, and others. Current research on formation control often relies on the use of an external motion capture equipment to track the pose of each individual robot. In order to deploy multiple Unmanned Aerial Vehicles (UAVs) to arbitrary environments, an alternative to motion capture or differential GPS is needed. In this paper, we show a proof of concept of a bearing-only relative localization approach in the context of maintaining flying formations, by recovering the 6 Degree of Freedom (DoF) relative pose between a pair of flying vehicles. This relative pose, estimated from a pair of images from mutually observing robots, is directly used to guide a follower quadrotor to keep a fixed position with respect to a leader. Experimental results from indoor and outdoor flying of two off-the-shelf quadrotors (Parrot AR.Drone 2.0) are presented.

[Link the the paper in pdf format](http://www2.ift.ulaval.ca/~pgiguere/papers/ARdroneCL_Workshop.2015.pdf)
